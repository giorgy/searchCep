package com.oliveira.giorgy.providerceo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import model.CEP;
import service.serviceSearchCep;

public class ProviderCepActivity extends AppCompatActivity {
//    criando os elementos para receber itens do fomulario
    EditText edtCep;
    TextView tvPrintSearchCep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_cep);

//        associando os elementos do meu laytou
        this.edtCep.findViewById(R.id.edt_search_cep);
        this.tvPrintSearchCep.findViewById(R.id.tv_search_cep_print_cep);
    }

    public void searchCep(View view) {
        String cep = this.edtCep.toString();

        try {
            CEP returnCep = new serviceSearchCep(cep).execute().get();
            tvPrintSearchCep.setText(returnCep.toString());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }
}
