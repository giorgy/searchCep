package service;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;
import model.CEP;

public class serviceSearchCep extends AsyncTask<Void, Void, CEP> {

    private final String cep;

    public serviceSearchCep(String cep) {
        if (cep != null && cep.length() == 8){
            this.cep = cep;
        } else {
            throw new IllegalArgumentException("CEP inválido");
        }

    }


    @Override
    protected CEP doInBackground(Void... voids) {
        Log.i("AST", "doInBackground");
        StringBuilder returnCep = new StringBuilder();


        try {
            URL url = new URL("https://viacep.com.br/ws/"+this.cep+"/json/");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("content-type","application/json");
            connection.setRequestProperty("accept","applications/json");
            connection.connect();

            //Scaneia a informação que recebeu e passa linha por linha
            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNext()){
                returnCep.append(scanner.next());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i("AST",returnCep.toString());

        //Importando a dependencia GSon que tranforma informações Json em um Objeto
        //GSon(recebe json, objeto a ser tranformado)
        CEP cep = new Gson().fromJson(returnCep.toString(), CEP.class);

        return cep;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Executa antes de dar inicio a uma nova thread
        Log.i("AST", "onPreExecute");

    }

    @Override
    protected void onPostExecute(CEP cep) {
        super.onPostExecute(cep);
        //Executa após a finalização da
        Log.i("AST", "onPostExecute");
    }




}
